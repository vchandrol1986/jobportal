package org.vl.jobportal.controller;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.vl.jobportal.businessprocessor.LoginBP;
import org.vl.jobportal.businessprocessor.impl.LoginBPImpl;
import org.vl.jobportal.model.LoginModel;
import org.vl.jobportal.utility.LoginUtility;
import org.vl.jobportal.validation.LoginValidation;

import java.util.logging.Logger;
/**
 * Servlet implementation class SeekerRegister
 */
public class SeekerRegister extends HttpServlet {
	private final LoginBP loginBP;
	private final LoginUtility loginUtility;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SeekerRegister() {
		loginBP = new LoginBPImpl();
		loginUtility = new LoginUtility();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		LOG.info("START:service(-,-)");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String fullname = request.getParameter("fullname");
		String designation = request.getParameter("designation");
		String contact = request.getParameter("contact");
		String companyname = request.getParameter("companyname");
		String companyaddress = request.getParameter("companyaddress");
		String companyprofile = request.getParameter("companyprofile");
		String mobile = request.getParameter("mobile");
		String city = request.getParameter("city");
		String gender = request.getParameter("gender");
		String passport = request.getParameter("passport");
		String address = request.getParameter("address");
		int roleid = 2;
		
		LOG.info("username:" + username);
		LOG.info("password:" + password);
		LOG.info("fullname:" + fullname);
		LOG.info("designation:" + designation);
		LOG.info("contact:" + contact);
		LOG.info("companyname:" + companyname);
		LOG.info("companyaddress:" + companyaddress);
		LOG.info("companyprofile:" + companyprofile);
		LOG.info("mobile:" + mobile);
		LOG.info("city:" + city);
		LOG.info("gender:" + gender);
		LOG.info("passport:" + passport);
		LOG.info("address:" + address);
		LOG.info("roleId:" + roleid);

		LoginModel loginModel = new LoginModel();
		loginModel.setUsername(username);
		loginModel.setPassword(password);
		loginModel.setFullName(fullname);
		loginModel.setDesignation(designation);
		loginModel.setContact(contact);
		loginModel.setCompanyName(companyname);
		loginModel.setCompanyProfile(companyprofile);
		loginModel.setMobile(mobile);
		loginModel.setCity(city);
		loginModel.setGender(gender);
		loginModel.setPassport(passport);
		loginModel.setAddress(address);
		loginModel.setRoleId(roleid);
		
		if (LoginValidation.isSeekerRegisterValid(loginModel) == Boolean.FALSE) {

		}
		else
		{
			boolean isValidCredential = loginBP.doLogin(loginModel);
			LOG.info("isValidCredential:"+isValidCredential);
		}

		response.sendRedirect("login.html");
	}
	
	
	private final Logger LOG =
			Logger.getLogger(SeekerRegister.class.getName());
}
