package org.vl.jobportal.model;

public class JobPostModel {
	
	private String jobcode;
	private String companyname;
	private String jobtitle;
	private String skillrequired;
	private String location;
	private String jobdescription;
	private String postdate;
	private String expiredate;
	
	
	
	
	public String getJobCode(){
		return jobcode;
	}
	public void  setJobCode(String jobcode){
		 this.jobcode=jobcode;
	}
	
	public String getCompanyName(){
		return companyname;
		
	}
	public void setCompanyName(String companyname){
		this.companyname=companyname;
	}
	public String getJobTitle(){
		return jobtitle;
	}
	public void setJobTitle(String jobtitle){
		this.jobtitle=jobtitle;
	}
	
	public String getSkillRequired(){ 
		return skillrequired;
	}
	public void setSkillRequired(String skillrequired){
		this.skillrequired=skillrequired;
	}
	
	public String getLocation(){
		return location;
	}
	public void setLocation(String location){
		this.location=location;
	}
	
	public String getJobDescription(){
		return jobdescription;
	}
	public void setJobDescription(String jobdescription){
		this.jobdescription=jobdescription;
	}
	

	public String getPostdate() {
		return postdate;
	}
	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}
	public String getExpireDate() {
		return expiredate;
	}
	public void setExpireDate(String expiredate) {
		this.expiredate = expiredate;
	}
	
}
