
package org.vl.jobportal.model;

public class LoginModel {

	private String username;
	private String password;
	private String fullName;
	private String designation;
	private String contact;
	private String companyName;
	private String companyAddress;
	private String companyProfile;
	private String mobile;
	private String city;
	private String gender;
	private String passport;
	private String address;
	private int roleId;
	private String createDate;
	private String modifiedDate;
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyProfile() {
		return companyProfile;
	}

	public void setCompanyProfile(String companyProfile) {
		this.companyProfile = companyProfile;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	

	public String getAddress() {

		return address;
	}

	public void setAddress(String address) {

		this.address = address;
	}

	public int getRoleId() {

		return roleId;
	}

	public void setRoleId(int roleId) {

		this.roleId = roleId;
	}

	public String getCreateDate() {

		return createDate;
	}

	public void setCreateDate(String createDate) {

		this.createDate = createDate;
	}

	public String getModifiedDate() {

		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {

		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {

		return "LoginModel[username:" + username + ",password:" + password +
			",fullName:" + fullName + ",address:" +
			address + ",createDate:" + createDate + ",modifiedDate:" +
			modifiedDate + "]";
	}
}
