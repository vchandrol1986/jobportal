
package org.vl.jobportal.validation;

import java.util.logging.Logger;

import org.vl.jobportal.constant.StringPool;
import org.vl.jobportal.model.LoginModel;

public class LoginValidation {

	private static boolean hasError;

	public static boolean isLoginModelValid(LoginModel loginModel) {

		LOG.info("isLoginModelValid LoginModel::" + loginModel.toString());
		hasError = Boolean.TRUE;
		if (loginModel.getUsername().equalsIgnoreCase(StringPool.BLANK) ||
				loginModel.getPassword().equalsIgnoreCase(StringPool.BLANK)) {
			hasError = Boolean.FALSE;
		}
		return hasError;
	}
	public static boolean isProviderRegisterValid(LoginModel loginModel){
		LOG.info("isLoginModelValid LoginModel::" + loginModel.toString());
		hasError = Boolean.TRUE;
		if (loginModel.getUsername().equalsIgnoreCase(StringPool.BLANK) ||
				loginModel.getPassword().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getFullName().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getDesignation().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getContact().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getCompanyName().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getCompanyProfile().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getMobile().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getCity().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getGender().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getPassport().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getAddress().equalsIgnoreCase(StringPool.BLANK)
				)
		{
			hasError = Boolean.FALSE;
		}
		return hasError;
	}
	
	public static boolean isSeekerRegisterValid(LoginModel loginModel){
		LOG.info("isLoginModelValid LoginModel::" + loginModel.toString());
		hasError = Boolean.TRUE;
		if (loginModel.getUsername().equalsIgnoreCase(StringPool.BLANK) ||
				loginModel.getPassword().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getFullName().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getDesignation().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getContact().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getCompanyName().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getCompanyProfile().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getMobile().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getCity().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getGender().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getPassport().equalsIgnoreCase(StringPool.BLANK)||
				loginModel.getAddress().equalsIgnoreCase(StringPool.BLANK)
				)
		{
			hasError = Boolean.FALSE;
		}
		return hasError;
	}

	
	private static Logger LOG =
			Logger.getLogger(LoginValidation.class.getName());
}
