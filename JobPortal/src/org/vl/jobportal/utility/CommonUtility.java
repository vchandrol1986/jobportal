package org.vl.jobportal.utility;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.vl.jobportal.constant.LoginConstant;

public class CommonUtility {

	private static Calendar calendar;

	public static String getCurrentDateTime() {
		calendar = Calendar.getInstance();

		String currentDateTime = LoginConstant.SIMPLEDATEFORMAT.format(calendar
				.getTime());
		return currentDateTime;

	}

	public static String getExpireDateTime(int afterMinutes)
			throws ParseException {
		calendar = Calendar.getInstance();

		Date dateTime = LoginConstant.SIMPLEDATEFORMAT
				.parse(LoginConstant.SIMPLEDATEFORMAT.format(calendar.getTime()));
		calendar.setTime(dateTime);
		calendar.add(Calendar.MINUTE, afterMinutes);

		String expireDateTime = LoginConstant.SIMPLEDATEFORMAT.format(calendar
				.getTime());
		return expireDateTime;

	}

}
