package org.vl.jobportal.utility;

import java.sql.Connection;
import java.sql.DriverManager;

import org.vl.jobportal.constant.SQLConstant;

public class LoginUtility {

	private Connection connection;

	public Connection getConnection(String username, String password,
			String connectionUrl) {

		try {
			Class.forName(SQLConstant.MYSQL_CLASS_NAME);
			connection = DriverManager.getConnection(connectionUrl, username,
					password);
			return connection;
		} catch (Exception e) {
			return null;
		}

	}

}
