package org.vl.jobportal.businessprocessor.impl;

import javax.servlet.http.HttpServletRequest;

import org.vl.jobportal.businessprocessor.LoginBP;
import org.vl.jobportal.model.LoginModel;
import org.vl.jobportal.service.LoginService;
import org.vl.jobportal.service.impl.LoginServiceImpl;

public class LoginBPImpl implements LoginBP {

	private final LoginService loginService;

	public LoginBPImpl() {

		loginService = new LoginServiceImpl();
	}

	@Override
	public boolean doLogin(LoginModel loginModel) {

		return loginService.doLogin(loginModel);
	}

	@Override
	public LoginModel getUserByUsername(String username) {
		// TODO Auto-generated method stub
		return loginService.getUserByUsername(username);
	}

	@Override
	public String getForgetPasswordLink(HttpServletRequest request,
			String username) {
		// TODO Auto-generated method stub
		return loginService.getForgetPasswordLink(request, username);
	}

	@Override
	public boolean deleteTicketByUsername(String username) {
		return loginService.deleteTicketByUsername(username);
	}

	@Override
	public boolean doProviderRegister(LoginModel loginModel)
	{
		return loginService.doProviderRegister(loginModel);
	}
	
	@Override
	public boolean doSeekerRegister(LoginModel loginModel)
	{
		return loginService.doSeekerRegister(loginModel);
	}
	

}
