package org.vl.jobportal.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.vl.jobportal.constant.SQLConstant;
import org.vl.jobportal.constant.SQLQueries;
import org.vl.jobportal.model.LoginModel;
import org.vl.jobportal.service.LoginService;
import org.vl.jobportal.utility.CommonUtility;
import org.vl.jobportal.utility.LoginUtility;

import sun.misc.BASE64Encoder;

public class LoginServiceImpl implements LoginService {

	private final LoginUtility loginUtility;

	private PreparedStatement preparedStatement;

	public LoginServiceImpl() {

		loginUtility = new LoginUtility();
	}

	@Override
	public boolean doLogin(LoginModel loginModel) {

		try {
			Class.forName(SQLConstant.MYSQL_CLASS_NAME);
			Connection connection = DriverManager.getConnection(
					SQLConstant.JDBC_URL, SQLConstant.USERNAME,
					SQLConstant.PASSWORD);
			preparedStatement = connection
					.prepareStatement(SQLQueries.LOGIN_QUERY);
			preparedStatement.setString(1, loginModel.getUsername());
			preparedStatement.setString(2, loginModel.getPassword());

			ResultSet resultSet = preparedStatement.executeQuery();
			LOG.info("resultSet:" + resultSet);
			if (resultSet.next()) {
				return Boolean.TRUE;
			} else {
				LOG.info("error");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			LOG.info("Error:" + e.getMessage());
			return Boolean.FALSE;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			LOG.info("Error:" + e.getMessage());
			return Boolean.FALSE;
		}
		return Boolean.FALSE;

	}

	Logger LOG = Logger.getLogger(LoginServiceImpl.class.getName());

	@Override
	public LoginModel getUserByUsername(String requestedUsername) {
		LoginModel loginModel = new LoginModel();

		try {
			Class.forName(SQLConstant.MYSQL_CLASS_NAME);

			Connection connection = DriverManager.getConnection(
					SQLConstant.JDBC_URL, SQLConstant.USERNAME,
					SQLConstant.PASSWORD);
			preparedStatement = connection
					.prepareStatement(SQLQueries.GET_USER_BY_USERNAME);
			preparedStatement.setString(1, requestedUsername);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				String username = resultSet.getString("username");
				String password = resultSet.getString("password");
				loginModel.setUsername(username);
				loginModel.setPassword(password);
				return loginModel;
			}
		} catch (SQLException e) {
			LOG.info("Error:" + e.getMessage());
			return null;
		} catch (ClassNotFoundException e) {
			LOG.info("Error:" + e.getMessage());
			return null;
		}
		return null;
	}

	@Override
	public String getForgetPasswordLink(HttpServletRequest request,
			String username) {

		try {
			Class.forName(SQLConstant.MYSQL_CLASS_NAME);
			Connection connection = DriverManager.getConnection(
					SQLConstant.JDBC_URL, SQLConstant.USERNAME,
					SQLConstant.PASSWORD);
			System.out.println(username + "::Connection is null "
					+ (connection == null));
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQLQueries.GET_USER_BY_USERNAME);
			preparedStatement.setString(1, username);
			ResultSet result = preparedStatement.executeQuery();
			if (result.next()) {

				String currentDateTime = CommonUtility.getCurrentDateTime();
				String expireDateTime = CommonUtility.getExpireDateTime(5);

				System.out.println("username:" + username);
				System.out.println("expireDateTime:" + expireDateTime);
				System.out.println("createDate:" + currentDateTime);

				String key = username.toString() + "#" + currentDateTime + "#"
						+ expireDateTime;

				log.info("key::" + key);

				key = new BASE64Encoder().encode(key.getBytes());

				String url = request.getScheme() + "://"
						+ request.getServerName() + ":"
						+ request.getServerPort() + request.getContextPath();
				url = url + "/forgotpassword1?key=" + key;

				log.info("generate URL : " + url);
				connection.close();
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection(SQLConstant.JDBC_URL,
						SQLConstant.USERNAME, SQLConstant.PASSWORD);

				preparedStatement = connection
						.prepareStatement(SQLQueries.TICKET_GENERATE);

				preparedStatement.setString(1, username);
				preparedStatement.setString(2, key);
				preparedStatement.setString(3, currentDateTime);
				preparedStatement.setString(4, expireDateTime);

				int updateCount = preparedStatement.executeUpdate();
				if (updateCount > 0) {
					return url;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private final Logger log = Logger.getLogger(LoginServiceImpl.class
			.getName());

	@Override
	public boolean deleteTicketByUsername(String username) {
		try {
			Class.forName(SQLConstant.MYSQL_CLASS_NAME);
			Connection connection = DriverManager.getConnection(
					SQLConstant.JDBC_URL, SQLConstant.USERNAME,
					SQLConstant.PASSWORD);
			System.out.println(username + "::Connection is null "
					+ (connection == null));
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQLQueries.GET_USER_BY_USERNAME);
			preparedStatement.setString(1, username);
			ResultSet result = preparedStatement.executeQuery();
			if (result.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean doProviderRegister(LoginModel loginModel) {
		try {
			Class.forName(SQLConstant.MYSQL_CLASS_NAME);
			Connection connection = DriverManager.getConnection(
					SQLConstant.JDBC_URL, SQLConstant.USERNAME,
					SQLConstant.PASSWORD);

			PreparedStatement preparedStatement = connection
					.prepareStatement(SQLQueries.PROVIDER_REGISTER);



			preparedStatement.setString(1, loginModel.getUsername());
			preparedStatement.setString(2, loginModel.getPassword());
			preparedStatement.setString(3, loginModel.getFullName());
			preparedStatement.setString(4, loginModel.getDesignation());
			preparedStatement.setString(5, loginModel.getContact());
			preparedStatement.setString(6, loginModel.getCompanyName());
			preparedStatement.setString(7, loginModel.getCompanyAddress());
			preparedStatement.setString(8, loginModel.getCompanyProfile());
			preparedStatement.setString(9, loginModel.getMobile());
			preparedStatement.setString(10, loginModel.getCity());
			preparedStatement.setString(11, loginModel.getGender());
			preparedStatement.setString(12, loginModel.getPassport());
			preparedStatement.setString(13, loginModel.getAddress());
			preparedStatement.setInt(14, loginModel.getRoleId());

			preparedStatement.executeQuery();

			return preparedStatement.executeUpdate()>0?Boolean.TRUE:Boolean.FALSE;
		}

		catch (Exception e) {

		}

		return false;
	}

	public boolean doSeekerRegister(LoginModel loginModel) {
		try {
			Class.forName(SQLConstant.MYSQL_CLASS_NAME);
			Connection connection = DriverManager.getConnection(
					SQLConstant.JDBC_URL, SQLConstant.USERNAME,
					SQLConstant.PASSWORD);

			PreparedStatement preparedStatement = connection
					.prepareStatement(SQLQueries.PROVIDER_REGISTER);



			preparedStatement.setString(1, loginModel.getUsername());
			preparedStatement.setString(2, loginModel.getPassword());
			preparedStatement.setString(3, loginModel.getFullName());
			preparedStatement.setString(4, loginModel.getDesignation());
			preparedStatement.setString(5, loginModel.getContact());
			preparedStatement.setString(6, loginModel.getCompanyName());
			preparedStatement.setString(7, loginModel.getCompanyAddress());
			preparedStatement.setString(8, loginModel.getCompanyProfile());
			preparedStatement.setString(9, loginModel.getMobile());
			preparedStatement.setString(10, loginModel.getCity());
			preparedStatement.setString(11, loginModel.getGender());
			preparedStatement.setString(12, loginModel.getPassport());
			preparedStatement.setString(13, loginModel.getAddress());
			preparedStatement.setInt(14, loginModel.getRoleId());

			preparedStatement.executeQuery();

			return preparedStatement.executeUpdate()>0?Boolean.TRUE:Boolean.FALSE;
		}

		catch (Exception e) {

		}

		return false;
	}

}
