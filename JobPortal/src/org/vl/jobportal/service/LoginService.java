package org.vl.jobportal.service;

import javax.servlet.http.HttpServletRequest;

import org.vl.jobportal.model.LoginModel;

public interface LoginService {

	public boolean doLogin(LoginModel loginModel);

	public LoginModel getUserByUsername(String username);

	public String getForgetPasswordLink(HttpServletRequest request,
			String username);

	public boolean deleteTicketByUsername(String username);

	public boolean doProviderRegister(LoginModel loginModel);
	
	public boolean doSeekerRegister(LoginModel loginModel);
}
