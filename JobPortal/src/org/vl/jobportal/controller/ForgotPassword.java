package org.vl.jobportal.controller;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.vl.jobportal.businessprocessor.LoginBP;
import org.vl.jobportal.businessprocessor.impl.LoginBPImpl;
import org.vl.jobportal.utility.LoginUtility;

public class ForgotPassword extends HttpServlet {

	private final LoginBP loginBP;
	private final LoginUtility loginUtility;

	public ForgotPassword() {

		loginBP = new LoginBPImpl();
		loginUtility = new LoginUtility();

	}

	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String username = request.getParameter("username");
		System.out.println("Username:" + username);

		String generatedURL = loginBP.getForgetPasswordLink(request, username);

		if (generatedURL != null)
		{
			request.getSession().setAttribute("key", generatedURL);
		}
		else
		{
			request.getSession().setAttribute("error", "Error: Server Side.");
		}

		response.sendRedirect("forgotpassword.jsp");

	}

	private final Logger log = Logger.getLogger(ForgotPassword.class.getName());

}
