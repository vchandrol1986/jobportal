package org.vl.jobportal.controller;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.vl.jobportal.businessprocessor.LoginBP;
import org.vl.jobportal.businessprocessor.impl.LoginBPImpl;
import org.vl.jobportal.utility.LoginUtility;

/**
 * Servlet implementation class JobApplied
 */
public class JobApplied extends HttpServlet {
	private final LoginBP loginBP;
	private final LoginUtility loginUtility;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public JobApplied() {
    	loginBP = new LoginBPImpl();
		loginUtility = new LoginUtility();
        // TODO Auto-generated constructor stub
    }
    
    protected void service(
			HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {
    	
    }

    private final Logger LOG =
			Logger.getLogger(JobApplied.class.getName());
	

}
