package org.vl.jobportal.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.vl.jobportal.businessprocessor.LoginBP;
import org.vl.jobportal.businessprocessor.impl.LoginBPImpl;
import org.vl.jobportal.constant.StringPool;
import org.vl.jobportal.model.LoginModel;
import org.vl.jobportal.utility.CommonUtility;
import org.vl.jobportal.utility.LoginUtility;

import sun.misc.BASE64Decoder;

public class ForgotPasswordController extends HttpServlet {
	private final LoginBP loginBP;
	private final LoginUtility loginUtility;

	public ForgotPasswordController() {

		loginBP = new LoginBPImpl();
		loginUtility = new LoginUtility();

	}

	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String key = request.getParameter("key");

		System.out.println("Key:" + key);
		BASE64Decoder base64Decoder = new BASE64Decoder();

		key = new String(base64Decoder.decodeBuffer(key));

		if (!key.equalsIgnoreCase(StringPool.BLANK)) {
			String content[] = key.split("#");

			String username = content[0];
			String createDateTime = CommonUtility.getCurrentDateTime();
			String expireDateTime = content[2];

			Date expireDate = null;
			Date currentDate = null;
			try {
				expireDate = new SimpleDateFormat("dd/MM/yyyy HH:mm")
						.parse(expireDateTime);
				currentDate = new SimpleDateFormat("dd/MM/yyyy HH:mm")
						.parse(createDateTime);
			} catch (ParseException e) {
				log.info(e.getMessage());
				e.printStackTrace();
			}
			log.info("decode username : " + username);
			log.info("decode createdatetime : " + createDateTime);
			log.info("decode expiredatetime : " + expireDateTime);
			if (currentDate.getTime() < expireDate.getTime()) {

				LoginModel loginModel = loginBP.getUserByUsername(username);
				String oldPassword = loginModel.getPassword();
				String username1 = loginModel.getUsername();
				request.getSession().setAttribute("username", username1);
				request.getSession().setAttribute("oldpassword", oldPassword);

				boolean isTicketResolve = loginBP
						.deleteTicketByUsername(username1);

				log.info("ticket Delete From Table : " + isTicketResolve);
				request.getSession().setAttribute("key-expired", "");
			} else {

				request.getSession().setAttribute("key-expired",
						"Sorry,your token has been expired.");

			}
			response.sendRedirect("resetpassword.jsp");

		}
	}

	Logger log = Logger.getLogger(ForgotPasswordController.class.getName());
}
