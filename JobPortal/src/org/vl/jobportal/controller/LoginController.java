
package org.vl.jobportal.controller;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.vl.jobportal.businessprocessor.LoginBP;
import org.vl.jobportal.businessprocessor.impl.LoginBPImpl;
import org.vl.jobportal.model.LoginModel;
import org.vl.jobportal.utility.LoginUtility;
import org.vl.jobportal.validation.LoginValidation;

public class LoginController extends HttpServlet {

	private final LoginBP loginBP;
	private final LoginUtility loginUtility;

	public LoginController() {

		loginBP = new LoginBPImpl();
		loginUtility = new LoginUtility();

	}

	@Override
	protected void service(
			HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {

		LOG.info("START:service(-,-)");
		int roleId = Integer.valueOf(request.getParameter("role"));
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		LOG.info("roleId:" + roleId);
		LOG.info("username:" + username);
		LOG.info("password:" + password);

		LoginModel loginModel = new LoginModel();
		loginModel.setUsername(username);
		loginModel.setPassword(password);
		loginModel.setRoleId(roleId);

		// check where login module is valid or not
		if (LoginValidation.isLoginModelValid(loginModel) == Boolean.FALSE) {

		}
		else {
			// check username and password whether correct or not
			boolean isValidCredential = loginBP.doProviderRegister(loginModel);

			LOG.info("isValidCredential:"+isValidCredential);
			if (isValidCredential) {

				request.getSession().setAttribute("username", username);
				if(roleId==1){
					response.sendRedirect("jobseekerhome.jsp");
				}else if(roleId==2){
					response.sendRedirect("jobproviderhome.jsp");
				}else{
					response.sendRedirect("adminhome.jsp");
				}



			}
			else {
				// means username and password incorrect
				response.sendRedirect("login.html");
			}

		}

		LOG.info("END:service(-,-)");

	}

	private final Logger LOG =
			Logger.getLogger(LoginController.class.getName());

}
