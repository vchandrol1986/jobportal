package org.vl.jobportal.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class job_post extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public job_post() {
        super();
      
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	response.setContentType("text/html");
	PrintWriter out=response.getWriter();
	
	String jcode=request.getParameter("jobcode");
	String Cname=request.getParameter("jobcompany");
	String title=request.getParameter("jobtitle");
	String skill=request.getParameter("jobskill");
	String loc=request.getParameter("jobloc");
	String jdes=request.getParameter("jobreq");
	String pdate=request.getParameter("postdate");
	String edate=request.getParameter("jobloc");
	
	try{  
		Class.forName("com.mysql.jdbc.Driver");  
		Connection con=DriverManager.getConnection(  
		"jdbc:mysql://localhost:3306/jobportal","root","root");  
		  
		PreparedStatement ps=con.prepareStatement(  
				"insert into jobpost values(?,?,?,?,?,?,?,?)");
		
		ps.setString(1, jcode);
		ps.setString(2, Cname);
		ps.setString(3,title );
		ps.setString(4,skill );
		ps.setString(5,loc );
		ps.setString(6, jdes);
		ps.setString(7, pdate);
		ps.setString(8,edate );
	     
		int i=ps.executeUpdate();  
		if(i>0)  
		out.print("You are successfully registered...");
		
		
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	out.close();

}
}