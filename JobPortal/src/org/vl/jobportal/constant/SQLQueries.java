package org.vl.jobportal.constant;

public class SQLQueries {

	public static final String LOGIN_QUERY = "select * from logintab where username =? and password=?";

	public static final String GET_USER_BY_USERNAME = "select * from logintab where username=?";

	public static final String DELETE_TICKET_BY_USERNAME = "DELETE FROM tickettab  where username=?";

	public static final String TICKET_GENERATE = "insert into tickettab(username,deeplink,createDate,expireDate) values(?,?,?,?)";

	public static final String PROVIDER_REGISTER = "insert into logintab(username,password,fullname,designation,contact,companyname,companyaddress,companyprofile,mobile,city,gender,passport,address,roleid) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
}
