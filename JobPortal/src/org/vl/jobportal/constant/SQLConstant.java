package org.vl.jobportal.constant;

public class SQLConstant {

	public static final String MYSQL_CLASS_NAME = "com.mysql.jdbc.Driver";
	public static final String USERNAME = "root";
	public static final String PASSWORD = "root";
	public static final String JDBC_URL = "jdbc:mysql://localhost/jobportal";
}
