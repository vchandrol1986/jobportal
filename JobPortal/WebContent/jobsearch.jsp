<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.vl.jobportal.constant.SQLConstant"%>
<%@page import="org.vl.jobportal.utility.LoginUtility"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Job Portal</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Seeking Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel='stylesheet'
	type='text/css' /> -->
	<link href="css/bootstrap-3.1.1.min.css" rel='stylesheet'
	type='text/css' />
	
<link href="css/dataTables.bootstrap.min.css" rel='stylesheet' type='text/css' />	
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link
	href='//fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<!----font-Awesome----->
<link href="css/font-awesome.css" rel="stylesheet">
<!----font-Awesome----->
</head>
<body>
	<%
	//remove below line after completing main code
	int i=0;
  if(session.getAttribute("username")!=null || i==0){
	  
	  LoginUtility loginUtility=new LoginUtility();
	  Connection connection= loginUtility.getConnection(SQLConstant.USERNAME, SQLConstant.PASSWORD, SQLConstant.JDBC_URL);
	  
	  Statement statement= connection.createStatement();
	  
	  ResultSet resultSet= statement.executeQuery("select * from jobpost");
	 
	  
	  
%>
	<nav class="navbar navbar-default" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"
					style="padding-top: 20px; color: white; font-weight: bold;">JOB
					PORTAL</a>
			</div>
			<!--/.navbar-header-->
			<div class="navbar-collapse collapse"
				id="bs-example-navbar-collapse-1" style="height: 1px;">
				<ul class="nav navbar-nav">

					<li><a href="createresume.html">Create Resume</a>
					</li>
					<li><a href="jobsearch.jsp">Job Search</a>
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		<!--/.navbar-collapse-->
		<span
			style="float: right; margin-left: 35px; color: white; text-transform: capitalize;"><img
			src="smalluser.jpg" style="border-radius: 50%;">Hello,<%=(String)session.getAttribute("username") %>
		</span>
	</nav>

	<div style="padding-bottom: 50px;">
		<div class="row" style="margin: 50px -15px;">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="table-responsive">
				   <span>Available Jobs</span>
					<table id="example" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
							<tr>
							    <th>#</th>
							    <th>Id</th>
							    <th>Job Code</th>
								<th>Company Name</th>
								<th>Job Title</th>
								<th>Skill Required</th>
								<th>Location</th>
								<th>Job Description</th>
								<th>Post Date</th>
								<th>Expired Date</th>
				
								<th></th>
							</tr>
						</thead>
						
						<tbody>
						
						<%
						 while(resultSet.next()){
							  String companyName=resultSet.getString(3);
						      String  id = resultSet.getString(1);
						      String jobCode = resultSet.getString(2);
						      String jobTitle = resultSet.getString(4);
						      String skillRequired= resultSet.getString(5);
						      String location = resultSet.getString(6);
						      String jobDescription = resultSet.getString(7);
						      String postDate = resultSet.getString(8);
						      String expiredDate = resultSet.getString(9);
						  
						%>
						
						
							<tr>
							   
				               
								<td></td>
								<td><%=id %></td>
								<td><%=jobCode%></td>
								<td>
								  <a href="#" style="text-decoration: underline;" title="view this company">
								    <%=companyName%>
								  </a>
								  </td>
								<td><%=jobTitle%></td>
								<td><%=skillRequired%></td>
								<td><%=location%></td>
								<td><%=jobDescription%></td>
								<td><%=postDate%></td>
								<td><%=expiredDate%></td>
								<td>
								   <button type="button" class="btn btn-danger" value="Apply Now"> APPLY NOW
								</td>
							</tr>
							
							
						
							<%} %>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-2"></div>

		</div>

	</div>

	<div class="footer_bottom" style="height: 30px; padding-bottom: 0px; padding-top: 0px;">
		<div class="container">

			<div class="clearfix"></div>
			<div class="copy" style="margin-top: 5px;">
				<p>Copyright &copy; 2016 Job Portal . All Rights Reserved . Design
					by</p>
			</div>
		</div>
	</div>
	<%}else{ 
   response.sendRedirect("login.html");
} %>
</body>
</html>
