<%@page import="org.vl.jobportal.constant.StringPool"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function checkError(){
  
	var pwd1=document.getElementById('pwd1').value;
	var pwd2=document.getElementById('pwd2').value;
    if(pwd1!=pwd2){
		alert("sorry,password mismatch!!");
		document.getElementById('pwd1').value="";
		document.getElementById('pwd2').value="";
		document.getElementById('pwd1').focus();
    	return false	;
    }else if(pwd1=="" ||pwd2==""  ){
    	alert("password should not empty.")
    	document.getElementById('pwd1').value="";
		document.getElementById('pwd2').value="";
		document.getElementById('pwd1').focus();
    	return false;
    }
    
}
</script>
</head>
<body>

<%
  String error=StringPool.BLANK;
try{
  if(session.getAttribute("key-expired").toString()!=null ){
	  error=session.getAttribute("key-expired").toString();
  }
}catch(Exception e){
	error=StringPool.BLANK;
}
  if(!error.equalsIgnoreCase(StringPool.BLANK)){

%>

<span style="color:red;"><%=error %></span>

<%}else{
   String oldPassword=session.getAttribute("oldpassword").toString();
   String username=session.getAttribute("username").toString();

 %>

<strong>Hey <%=username %></strong>,please try to change your password.

<form action="resetPasssword" method="post" onsubmit="return checkError();">

   <input type="hidden" name="username" value="<%=username%>"/>
   
   <table>
      <tr>
         <td>Password</td>
         <td> <input type="password" name="newpassword" id="pwd1"/></td>
      </tr>
      <tr>
         <td>Confirm Password</td>
         <td> <input type="password" name="oldpassword" id="pwd2"/></td>
      </tr>
      <tr>
         <td>  <input type="submit" value="Reset Password"/></td>
         <td>  <input type="submit" value="Reset"/></td>
      </tr>
   
   </table>
  
  
  
 
</form>

<%} %>

</body>
</html>